import random
from anytree import Node, RenderTree
from anytree.exporter import DotExporter
import numpy as np
from sklearn.linear_model import LinearRegression
from math import sin, cos


class REC:
    def __init__(self):
        # configs
        self.tree_nodes = 5
        self.data = []
        self.tree = []
        self.operators = ['c', 's', 'p', '+', '-', '/']
        self.operators_chd = {'c': 1, 's': 1, 'p': 2, '+': 2, '-': 2, '/': 2}
        self.operands = [random.uniform(-2, 3), random.uniform(-5, 5)]
        self.tree = None
        # self.read_data()
        # _input = []
        # for chunk in self.data:
        #     _input.append([chunk[0], chunk[1]])
        # _output = []
        # for chunk in self.data:
        #     _output.append(chunk[2])
        # self.inputs = np.array(_input).reshape((-1, 2))
        # self.outputs = np.array(_output)
        # print(self.inputs)
        # print(self.outputs)

    def read_data(self):
        raw_data = open('data.txt', 'r')
        while 1:
            _temp = raw_data.readline()
            if not _temp:
                break
            _temp = _temp.split(',')
            _data = []
            for item in _temp:
                _data.append(float(item))
            self.data.append(_data)

    def rnd_num(self):
        _random_param = random.randint(0, 1)
        return self.operands[_random_param]

    def create_tree(self):
        tree_nodes = self.tree_nodes
        _tree = [Node('root')]
        # fill operators
        while True:
            if tree_nodes <= 1:
                break
            _opt = random.randint(0, len(self.operators)-1)
            _opt = self.operators[_opt]
            _opt_chd = self.operators_chd[_opt]
            if tree_nodes < _opt_chd + 1:
                continue
            # for first time
            if len(_tree) == 1:
                _tree.append(Node(_opt, parent=_tree[0]))
                tree_nodes -= _opt_chd
                continue
            _free_nodes = []
            for node in range(1, len(_tree)):
                _node_avail_chd = self.operators_chd[_opt]
                if len(_tree[node].children) < _node_avail_chd:
                    _free_nodes.append(node)
            _picked_node_idx = _free_nodes[random.randint(0, len(_free_nodes)-1)]
            _tree.append(Node(_opt, parent=_tree[_picked_node_idx]))
            tree_nodes -= _opt_chd

            # fill numbers
            # while True:
            #     _operand = self.operands[random.randint(0, len(self.operands) - 1)]
            #     _free_nodes = []
            #     for node in range(1, len(_tree)):
            #         if _tree[node].name in self.operands:
            #             continue
            #         _node_avail_chd = self.operators_chd[_tree[node].name]
            #         if len(_tree[node].children) < _node_avail_chd:
            #             _free_nodes.append(node)
            #     if len(_free_nodes) == 0:
            #         break
            #     _picked_node_idx = _free_nodes[random.randint(0, len(_free_nodes) - 1)]
            #     _tree.append(Node(_operand, parent=_tree[_picked_node_idx]))
            while True:
                _operand = self.rnd_num()
                _free_nodes = []
                for node in range(1, len(_tree)):
                    if _tree[node].name not in self.operators:
                        continue
                    _node_avail_chd = self.operators_chd[_tree[node].name]
                    if len(_tree[node].children) < _node_avail_chd:
                        _free_nodes.append(node)
                if len(_free_nodes) == 0:
                    break
                _picked_node_idx = _free_nodes[random.randint(0, len(_free_nodes) - 1)]
                _tree.append(Node(_operand, parent=_tree[_picked_node_idx]))

        self.tree = _tree

    def render_tree(self, node_id='root'):
        if node_id == 'root':
            return self.render_tree(self.tree[0].children[0])

        for node in self.tree:
            # print('*' * 100)
            # print(node)
            if node == node_id:
                if len(node.children) > 1:
                    if node.children[0].name not in self.operators and node.children[1].name not in self.operators:
                        return self.render_operator(node.name, node.children[0].name, node.children[1].name)
                    if node.children[0].name not in self.operators and node.children[1].name in self.operators:
                        return self.render_operator(node.name, node.children[0].name,
                                                    self.render_tree(node.children[1]))
                    if node.children[0].name in self.operators and node.children[1].name not in self.operators:
                        return self.render_operator(node.name, self.render_tree(node.children[0]),
                                                    node.children[1].name)
                else:
                    # print(node.children[0].name)
                    if node.children[0].name not in self.operators:
                        return self.render_operator(node.name, node.children[0].name)
                    if node.children[0].name in self.operators:
                        return self.render_operator(node.name, self.render_tree(node.children[0]))

        # model = LinearRegression().fit(self.inputs, self.outputs)
        # return model

    @staticmethod
    def render_operator(opt, ch1, ch2=None):
        if opt == 'c':
            return cos(ch1)
        if opt == 's':
            return sin(ch1)
        if opt == 'p':
            return ch1**ch2
        if opt == '+':
            return ch1 + ch2
        if opt == '-':
            return ch1 - ch2
        if opt == '/':
            return ch1 / ch2
        # if opt == 'root':
        #     return ch1

    def print_tree(self):
        print(RenderTree(self.tree[0]))

    ####################################################################################################################
    # EC part
    ####################################################################################################################
    def create_pop(self):
        pass


rec = REC()
rec.create_tree()
print(rec.render_tree())
rec.print_tree()


# DotExporter(tree[0]).to_picture("test.png")
# udo = Node("Udo")
# marc = Node("Marc", parent=udo)
# lian = Node("Lian", parent=marc)
# dan = Node("Dan", parent=udo)
# jet = Node("Jet", parent=dan)
# jan = Node("Jan", parent=dan)
# joe = Node("Joe", parent=dan)
# print(dan.parent)
# print(dan.children.index(0))
# print(dan.children[0])
