## As the name of the project says the aim of this project is to estimate formula for data series with a tree with this a spec:
### operators = cos, sin, power, +, -, /
#### You can change them super ez in code
***
### now lets jump on some output examples:
#### 0.8414709848078965
    Node('/root')
    └── Node('/root/s')
        └── Node('/root/s//')
            ├── Node('/root/s///-0.8897727839201686')
            │   └── Node('/root/s///-0.8897727839201686/c')
            │       └── Node('/root/s///-0.8897727839201686/c/-0.8897727839201686')
            └── Node('/root/s///-0.8897727839201686')

#### -0.9957493662663656
    Node('/root')
    └── Node('/root/s')
        └── Node('/root/s/+')
            ├── Node('/root/s/+/-0.7392806587698129')
            └── Node('/root/s/+/-0.7392806587698129')
                └── Node('/root/s/+/-0.7392806587698129/s')
                    └── Node('/root/s/+/-0.7392806587698129/s/1.0425418320325903')

##### If you didn't understand this see some youtube on this subject first.
##### Hope you enjoy the code :D and super interesting concept
##### (by the way I left some of my tests and etc commented in code in too you can start by that parts)